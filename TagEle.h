#include <fstream>
#include <iostream>
#include <vector>
#include <TLorentzVector.h>
#include <TH1D.h>
#include <TRandom.h>
#include <TLegend.h>
#include <TCanvas.h>
#include </afs/cern.ch/work/j/jhihjia/private/untuplizer_07.h>
using namespace std;

void TagEle(TreeReader &data, vector<int> &Tag){

  Int_t nEle = data.GetInt("nEle");
  Float_t* elePt      = data.GetPtrFloat("elePt");
  Float_t* eleEta     = data.GetPtrFloat("eleEta");
  Float_t* elePhi     = data.GetPtrFloat("elePhi");

  UShort_t* eleIDbit = NULL;
  eleIDbit = (UShort_t*) data.GetPtrShort("eleIDbit");
  ULong64_t* eleFiredSingleTrgs_ = NULL;
  eleFiredSingleTrgs_ = (ULong64_t*)  data.GetPtrLong64("eleFiredSingleTrgs");

  Float_t HighPt = 0;
  Int_t TemTagNum = 0;


  for(int i = 0; i < nEle; i++){
    if((eleIDbit[i]>>3&1) == 1){
      if ((eleFiredSingleTrgs_[i]>>11&1) == 1){
	if(eleEta[i] < 2.1) Tag.push_back(i); // turn on
      }
    }
  } // find tag ele

  //if(Tag.size() == 1) continue;
  if(Tag.size() > 1){
    HighPt = elePt[Tag[0]];
    TemTagNum = Tag[0];
    for(int i = 1; i < Tag.size(); i++){
      if(HighPt < elePt[Tag[i]]){
	HighPt = elePt[Tag[i]];
	TemTagNum = Tag[i];
      }
    }
    Tag.clear();
    Tag.push_back(TemTagNum);
  } // find the one tag
} 
