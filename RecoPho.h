#include <fstream>
#include <iostream>
#include <vector> 
#include <TLorentzVector.h>
#include <TH1D.h>
#include <TRandom.h>
#include <TLegend.h>
#include <TCanvas.h>
#include </afs/cern.ch/work/j/jhihjia/private/untuplizer_07.h>
using namespace std;

void RecoPho(TreeReader &data, vector<int> &ProNumber, vector<int> &PhoNumber, vector<int> &EBPhoNumber, vector<int> &EEPhoNumber, vector<int> &EBPhoNumberCut, vector<int> &EEPhoNumberCut){

  Int_t nEle = data.GetInt("nEle");
  Float_t* eleSCEta	= data.GetPtrFloat("eleSCEta");
  Float_t* eleSCPhi	= data.GetPtrFloat("eleSCPhi");
  Float_t* phoSCEta	= data.GetPtrFloat("phoSCEta");
  Float_t* phoSCPhi	= data.GetPtrFloat("phoSCPhi");
  Float_t* phoIDMVA	= data.GetPtrFloat("phoIDMVA");
  //Float_t* phoR9	= data.GetPtrFloat("phoSigmaIEtaIEtaFull5x5");

  Float_t ProSCEta, ProSCPhi, PhoSCEta, PhoSCPhi, TemPhoIDMVA;

  if(ProNumber.size() != 0){
    PhoSCEta = 0, PhoSCPhi = 0;
    for(int i = 0; i < ProNumber.size(); i++){
      ProSCEta = eleSCEta[ProNumber[i]];
      ProSCPhi = eleSCPhi[ProNumber[i]];
      for(int i = 0; i < nEle; i++){
	PhoSCEta = phoSCEta[i];
	PhoSCPhi = phoSCPhi[i];
	TemPhoIDMVA = phoIDMVA[i];
	if(ProSCEta != PhoSCEta) continue;
	if(ProSCPhi != PhoSCPhi) continue;
	if(fabs(PhoSCEta) < 1.4442){
	  PhoNumber.push_back(i);
	  EBPhoNumber.push_back(i);
	  if(TemPhoIDMVA > -0.02) EBPhoNumberCut.push_back(i);
	}
	if(fabs(PhoSCEta) > 1.566 && fabs(PhoSCEta) < 2.5){
	  PhoNumber.push_back(i);
	  EEPhoNumber.push_back(i);
	  if(TemPhoIDMVA > -0.26) EEPhoNumberCut.push_back(i);
	}
      }
    }
  }
}
