#include <fstream>
#include <iostream>
#include <vector>
#include <TLorentzVector.h>
#include <TH1D.h>
#include <TH1F.h>
#include <TRandom.h>
#include <TLegend.h>
#include <TCanvas.h>
#include </afs/cern.ch/work/j/jhihjia/private/untuplizer_07.h>
#include </afs/cern.ch/work/j/jhihjia/private/ztoee/TagEle.h>
#include </afs/cern.ch/work/j/jhihjia/private/ztoee/ProEle.h>
#include </afs/cern.ch/work/j/jhihjia/private/ztoee/RecoPho.h>

using namespace std;

void t5(){

	//TreeReader data("/data5/ggNtuples/V09_04_13_03/job_SingleEle_Run2016B_Legacy/ggtree_data_1.root");
	//TreeReader data2("/data6/ggNtuples/V09_04_13_04/job_summer16_DYJetsToLL_m50_aMCatNLO_ext2/ggtree_mc_1.root");
	//float_t w = 6225.42*5.75*1000/data2.GetEntriesFast()/2636;
	TreeReader data("/data5/ggNtuples/V09_04_13_03/job_SingleEle_Run2016B_Legacy/ggtree_data_*.root");
	TreeReader data2("/data6/ggNtuples/V09_04_13_04/job_summer16_DYJetsToLL_m50_aMCatNLO_ext2/ggtree_mc_*.root");
	float_t w = 6225.42*5.75*1000/data2.GetEntriesFast();

	TH1D* Zmass = new TH1D("Zmass", "Z mass distribution", 60, 60, 120);
	TH1D* TagPt = new TH1D("TagPt", "eleTag Pt distribution", 90, 0, 90);
	TH1D* ProPt = new TH1D("ProPt", "elePro Pt distribution", 90, 0, 90);
	TH1D* SCEta = new TH1D("SCEta", "Photon SCEta distribution", 100, -3, 3);
	TH1D* EBR9 = new TH1D("EBR9", "EB Photon R9 distribution", 100, 0, 1);
	TH1D* EER9 = new TH1D("EER9", "EE Photon R9 distribution", 100, 0, 1);
	TH1D* EBID = new TH1D("EBID", "EB phoIDMVA distribution", 200, -1, 1);
	TH1D* EEID = new TH1D("EEID", "EE phoIDMVA distribution", 200, -1, 1);
	TH1D* EBCutID = new TH1D("EBCutID", "EB phoIDMVA distribution(cut)", 200, -1, 1);
	TH1D* EECutID = new TH1D("EECutID", "EE phoIDMVA distribution(cut)", 200, -1, 1);
	TH1D* Sig = new TH1D("Sig", "photon phoSigmaIEtaIEtaFull5x5 distribution", 100, 0, 0.1);
	TH1D* EBSig = new TH1D("EBSig", "EB phoSigmaIEtaIEtaFull5x5 distribution(cut)", 250, 0, 0.025);
	TH1D* EESig = new TH1D("EESig", "EE phoSigmaIEtaIEtaFull5x5 distribution(cut)", 650, 0, 0.065);
	TH1D* EESigRR = new TH1D("EESigRR", "EE's Preshoer Sigma RR(cut)", 150, 0, 15);
	TH1D* EBEnRes = new TH1D("EB_EnRes", "EB's Energy resolution(cut)", 100, 0, 0.1);
	TH1D* EEEnRes = new TH1D("EE_EnRes", "EE's Energy resolution(cut)", 100, 0, 0.1);

	TH1D* mcZmass = new TH1D("Zmass", "mc Z mass distribution", 60, 60, 120);
	TH1D* mcTagPt = new TH1D("TagPt", "mc eleTag Pt distribution", 90, 0, 90);
	TH1D* mcProPt = new TH1D("ProPt", "mcelePro Pt distribution", 90, 0, 90);
	TH1D* mcSCEta = new TH1D("SCEta", "mc Photon SCEta distribution", 100, -3, 3);
	TH1D* mcEBR9 = new TH1D("EBR9", "mc EB Photon R9 distribution", 100, 0, 1);
	TH1D* mcEER9 = new TH1D("EER9", "mc EE Photon R9 distribution", 100, 0, 1);
	TH1D* mcEBID = new TH1D("EBID", "mc EB phoIDMVA distribution", 200, -1, 1);
	TH1D* mcEEID = new TH1D("EEID", "mc EE phoIDMVA distribution", 200, -1, 1);
	TH1D* mcEBCutID = new TH1D("EBCutID", "mc EB phoIDMVA distribution(cut)", 200, -1, 1);
	TH1D* mcEECutID = new TH1D("EECutID", "mc EE phoIDMVA distribution(cut)", 200, -1, 1);
	TH1D* mcSig = new TH1D("Sig", "mc photon phoSigmaIEtaIEtaFull5x5 distribution", 100, 0, 0.1);
	TH1D* mcEBSig = new TH1D("EBSig", "mc EB phoSigmaIEtaIEtaFull5x5 distribution(cut)", 250, 0, 0.025);
	TH1D* mcEESig = new TH1D("EESig", "mc EE phoSigmaIEtaIEtaFull5x5 distribution(cut)", 650, 0, 0.065);
	TH1D* mcEESigRR = new TH1D("mcEESigRR", "mc EE's Preshoer Sigma RR(cut)", 150, 0, 15);
	TH1D* mcEBEnRes = new TH1D("EB_EnRes", "mc EB's Energy resolution(cut)", 100, 0, 0.1);
	TH1D* mcEEEnRes = new TH1D("EE_EnRes", "mc EE's Energy resolution(cut)", 100, 0, 0.1);

	for (Long64_t ev = 0; ev < data.GetEntriesFast(); ev++){
	if(ev%500000 == 0) fprintf(stderr, "Processing event %lli of %lli \n", ev+1, data.GetEntriesFast());
	data.GetEntry(ev);
		
	Float_t* elePt	= data.GetPtrFloat("elePt");
    	Float_t* phoR9	= data.GetPtrFloat("phoR9");
    	Float_t* phoSCEta	= data.GetPtrFloat("phoSCEta");
    	Float_t* phoIDMVA	= data.GetPtrFloat("phoIDMVA");
    	Float_t* phoESEffSigmaRR	= data.GetPtrFloat("phoESEffSigmaRR");
    	Float_t* phoSigmaIEtaIEtaFull5x5	= data.GetPtrFloat("phoSigmaIEtaIEtaFull5x5");
    	Float_t* phoSigmaE	= data.GetPtrFloat("phoSigmaE");
    	Float_t* phoE	= data.GetPtrFloat("phoE");
    	
    	vector<int> TagNum, ProNum, ZMass, PhoNum, EBPhoNum, EEPhoNum;
    	vector<int> EBPhoNumCut, EEPhoNumCut;
    	ULong64_t HLTEleMuX_ = data.GetLong64("HLTEleMuX");
    	UShort_t* eleIDbit = NULL;
    	
    	TagNum.clear(), ProNum.clear(), ZMass.clear();
    	PhoNum.clear(), EBPhoNum.clear(), EEPhoNum.clear();
    	EBPhoNum.clear(), EEPhoNum.clear();
    	EBPhoNumCut.clear(), EEPhoNumCut.clear();
    	
    	if((HLTEleMuX_>>1&1) == 1){
      		TagEle(data, TagNum); 			// find tag electron number 
      		ProEle(data, TagNum, ProNum, ZMass); 	// find pro electron number and Z mass
      		RecoPho(data, ProNum, PhoNum, EBPhoNum, EEPhoNum, EBPhoNumCut, EEPhoNumCut);
    	}
    	
    	if(TagNum.size() != 0 && ProNum.size() != 0 && ZMass.size() != 0 && PhoNum.size() != 0){
      		Zmass->Fill(ZMass[0]);
      		TagPt->Fill(elePt[TagNum[0]]);
      		ProPt->Fill(elePt[ProNum[0]]);
      		SCEta->Fill(phoSCEta[PhoNum[0]]);
      		Sig->Fill(phoSigmaIEtaIEtaFull5x5[PhoNum[0]]);
      		if(EBPhoNum.size() != 0){
			EBID->Fill(phoIDMVA[EBPhoNum[0]]);
      			if(EBPhoNumCut.size() != 0){
	  				EBR9->Fill(phoR9[EBPhoNumCut[0]]);
	  				EBCutID->Fill(phoIDMVA[EBPhoNumCut[0]]);
	  				EBSig->Fill(phoSigmaIEtaIEtaFull5x5[EBPhoNumCut[0]]);
	  				EBEnRes->Fill(phoSigmaE[EBPhoNumCut[0]]/phoE[EBPhoNumCut[0]]);
				}
     		}
      		if(EEPhoNum.size() != 0){
			EEID->Fill(phoIDMVA[EEPhoNum[0]]);
      			if(EEPhoNumCut.size() != 0){
	  				EER9->Fill(phoR9[EEPhoNumCut[0]]);
 	  				EECutID->Fill(phoIDMVA[EEPhoNumCut[0]]);
	  				EESig->Fill(phoSigmaIEtaIEtaFull5x5[EEPhoNumCut[0]]);
	  				EESigRR->Fill(phoESEffSigmaRR[EEPhoNumCut[0]]);
	  				EEEnRes->Fill(phoSigmaE[EEPhoNumCut[0]]/phoE[EEPhoNumCut[0]]);
				}
     		}
    	}
  	}
  	fprintf(stderr, "Processed all event \n");

	//TreeReader data2("/data6/ggNtuples/V09_04_13_04/job_summer16_DYJetsToLL_m50_aMCatNLO_ext2/ggtree_mc_1.root");
	//float_t w = 6225.42*5.750*1000/data2.GetEntriesFast();
  	cout << w << endl;
  	
	for (Long64_t ev = 0; ev < data2.GetEntriesFast(); ev++){
    	if(ev%500000 == 0) fprintf(stderr, "Processing event %lli of %lli \n", ev+1, data2.GetEntriesFast());
    	data2.GetEntry(ev);
    	
    	Float_t* elePt	= data2.GetPtrFloat("elePt");
    	Float_t* phoR9	= data2.GetPtrFloat("phoR9");
    	Float_t* phoSCEta	= data2.GetPtrFloat("phoSCEta");
    	Float_t* phoIDMVA	= data2.GetPtrFloat("phoIDMVA");
    	Float_t* phoESEffSigmaRR	= data2.GetPtrFloat("phoESEffSigmaRR");
    	Float_t* phoSigmaIEtaIEtaFull5x5	= data2.GetPtrFloat("phoSigmaIEtaIEtaFull5x5");
    	Float_t* phoSigmaE	= data2.GetPtrFloat("phoSigmaE");
    	Float_t* phoE	= data2.GetPtrFloat("phoE");
    	
    	vector<int> TagNum, ProNum, ZMass, PhoNum, EBPhoNum, EEPhoNum;
    	vector<int> EBPhoNumCut, EEPhoNumCut;
    	ULong64_t HLTEleMuX_ = data2.GetLong64("HLTEleMuX");
    	UShort_t* eleIDbit = NULL;
    	
    	TagNum.clear(), ProNum.clear(), ZMass.clear();
    	PhoNum.clear(), EBPhoNum.clear(), EEPhoNum.clear();
    	EBPhoNum.clear(), EEPhoNum.clear();
    	EBPhoNumCut.clear(), EEPhoNumCut.clear();
    	
    	if((HLTEleMuX_>>1&1) == 1){
      		TagEle(data2, TagNum); 			// find tag electron number 
      		ProEle(data2, TagNum, ProNum, ZMass); 	// find pro electron number and Z mass
      		RecoPho(data2, ProNum, PhoNum, EBPhoNum, EEPhoNum, EBPhoNumCut, EEPhoNumCut);
    	}

    	if(TagNum.size() != 0 && ProNum.size() != 0 && ZMass.size() != 0 && PhoNum.size() != 0){
      		mcZmass->Fill(ZMass[0], w);
      		mcTagPt->Fill(elePt[TagNum[0]],w);
      		mcProPt->Fill(elePt[ProNum[0]],w);
      		mcSCEta->Fill(phoSCEta[PhoNum[0]],w);
      		mcSig->Fill(phoSigmaIEtaIEtaFull5x5[PhoNum[0]],w);
      		if(EBPhoNum.size() != 0){
			mcEBID->Fill(phoIDMVA[EBPhoNum[0]],w);
      			if(EBPhoNumCut.size() != 0){
	  				mcEBR9->Fill(phoR9[EBPhoNumCut[0]],w);
	  				mcEBCutID->Fill(phoIDMVA[EBPhoNumCut[0]],w);
	  				mcEBSig->Fill(phoSigmaIEtaIEtaFull5x5[EBPhoNumCut[0]],w);
	  				mcEBEnRes->Fill(phoSigmaE[EBPhoNumCut[0]]/phoE[EBPhoNumCut[0]],w);
				}
     		}
      		if(EEPhoNum.size() != 0){
			mcEEID->Fill(phoIDMVA[EEPhoNum[0]],w);
      			if(EEPhoNumCut.size() != 0){
	  				mcEER9->Fill(phoR9[EEPhoNumCut[0]],w);
 	  				mcEECutID->Fill(phoIDMVA[EEPhoNumCut[0]],w);
	  				mcEESig->Fill(phoSigmaIEtaIEtaFull5x5[EEPhoNumCut[0]],w);
	  				mcEESigRR->Fill(phoESEffSigmaRR[EEPhoNumCut[0]],w);
	  				mcEEEnRes->Fill(phoSigmaE[EEPhoNumCut[0]]/phoE[EEPhoNumCut[0]],w);
				}
     		}
    	}
  	}
  	fprintf(stderr, "Processed all event \n");

	Zmass->GetXaxis()->SetTitle("Mass of Z boson");
	Zmass->GetYaxis()->SetTitle("Entries");
	TagPt->GetXaxis()->SetTitle("Pt of tag's distribution");
	TagPt->GetYaxis()->SetTitle("Entries");
	ProPt->GetXaxis()->SetTitle("Pt of probe's distribution");
	ProPt->GetYaxis()->SetTitle("Entries");
	SCEta->GetXaxis()->SetTitle("the distribution of photon's SCEta");
	SCEta->GetYaxis()->SetTitle("Entries");
	EBR9->GetXaxis()->SetTitle("the distribution of EB photon's R9");
	EBR9->GetYaxis()->SetTitle("Entries");
	EER9->GetXaxis()->SetTitle("the distribution of EE photon's R9");
	EER9->GetYaxis()->SetTitle("Entries");
	EBID->GetXaxis()->SetTitle("the distribution of EB photon's IDMVA");
	EBID->GetYaxis()->SetTitle("Entries");
	EEID->GetXaxis()->SetTitle("the distribution of EE photon's IDMVA");
	EEID->GetYaxis()->SetTitle("Entries");
	EBCutID->GetXaxis()->SetTitle("the distribution of EB photon's IDMVA(cut)");
	EBCutID->GetYaxis()->SetTitle("Entries");
	EECutID->GetXaxis()->SetTitle("the distribution of EE photon's IDMVA(cut)");
	EECutID->GetYaxis()->SetTitle("Entries");
	Sig->GetXaxis()->SetTitle("the distribution of photon's phoSigmaIEtaIEtaFull5x5");
	Sig->GetYaxis()->SetTitle("Entries");
	EBSig->GetXaxis()->SetTitle("the distribution of EB photon's phoSigmaIEtaIEtaFull5x5");
	EBSig->GetYaxis()->SetTitle("Entries");
	EESig->GetXaxis()->SetTitle("the distribution of EE photon's phoSigmaIEtaIEtaFull5x5");
	EESig->GetYaxis()->SetTitle("Entries");
	EESigRR->GetXaxis()->SetTitle("the distribution of EE photon's Preshower Sigma RR");
	EESigRR->GetYaxis()->SetTitle("Entries");

	Int_t rMax = 2, RMax = 3;

	// Define the ratio plot
	TH1F *RZmass = (TH1F*)Zmass->Clone("RZmass");
	RZmass->SetLineColor(kBlack);
	RZmass->SetMinimum(0);  // Define Y ..
	RZmass->SetMaximum(RMax); // .. range
	RZmass->Sumw2();
	RZmass->SetStats(0);      // No statistics on lower plot
	RZmass->Divide(mcZmass);
	RZmass->SetMarkerStyle(21);
	RZmass->GetXaxis()->SetTitle("Mass of Z boson");
	RZmass->GetYaxis()->SetTitle("Ratio");
	
	TH1F *RTagPt = (TH1F*)TagPt->Clone("RTagPt");
	RTagPt->SetLineColor(kBlack);
	RTagPt->SetMinimum(0);  // Define Y ..
	RTagPt->SetMaximum(rMax); // .. range
	RTagPt->Sumw2();
	RTagPt->SetStats(0);      // No statistics on lower plot
	RTagPt->Divide(mcTagPt);
	RTagPt->SetMarkerStyle(21);
	RTagPt->GetXaxis()->SetTitle("Pt of tag's distribution");
	RTagPt->GetYaxis()->SetTitle("Ratio");
	
	TH1F *RProPt = (TH1F*)ProPt->Clone("RProPt");
	RProPt->SetLineColor(kBlack);
	RProPt->SetMinimum(0);  // Define Y ..
	RProPt->SetMaximum(rMax); // .. range
	RProPt->Sumw2();
	RProPt->SetStats(0);      // No statistics on lower plot
	RProPt->Divide(mcProPt);
	RProPt->SetMarkerStyle(21);
	RProPt->GetXaxis()->SetTitle("Pt of probe's distribution");
	RProPt->GetYaxis()->SetTitle("Ratio");
	
	TH1F *RSCEta = (TH1F*)SCEta->Clone("RSCEta");
	RSCEta->SetLineColor(kBlack);
	RSCEta->SetMinimum(0);  // Define Y ..
	RSCEta->SetMaximum(rMax); // .. range
	RSCEta->Sumw2();
	RSCEta->SetStats(0);      // No statistics on lower plot
	RSCEta->Divide(mcSCEta);
	RSCEta->SetMarkerStyle(21);
	RSCEta->GetXaxis()->SetTitle("the distribution of photon's SCEta");
	RSCEta->GetYaxis()->SetTitle("Ratio");
	
	TH1F *REBR9 = (TH1F*)EBR9->Clone("REBR9");
	REBR9->SetLineColor(kBlack);
	REBR9->SetMinimum(0);  // Define Y ..
	REBR9->SetMaximum(rMax); // .. range
	REBR9->Sumw2();
	REBR9->SetStats(0);      // No statistics on lower plot
	REBR9->Divide(mcEBR9);
	REBR9->SetMarkerStyle(21);
	REBR9->GetXaxis()->SetTitle("the distribution of EB photon's R9");
	REBR9->GetYaxis()->SetTitle("Ratio");
	
	TH1F *REER9 = (TH1F*)EER9->Clone("REER9");
	REER9->SetLineColor(kBlack);
	REER9->SetMinimum(0);  // Define Y ..
	REER9->SetMaximum(rMax); // .. range
	REER9->Sumw2();
	REER9->SetStats(0);      // No statistics on lower plot
	REER9->Divide(mcEER9);
	REER9->SetMarkerStyle(21);
	REER9->GetXaxis()->SetTitle("the distribution of EE photon's R9");
	REER9->GetYaxis()->SetTitle("Ratio");
	
	TH1F *REBID = (TH1F*)EBID->Clone("REBID");
	REBID->SetLineColor(kBlack);
	REBID->SetMinimum(0);  // Define Y ..
	REBID->SetMaximum(rMax); // .. range
	REBID->Sumw2();
	REBID->SetStats(0);      // No statistics on lower plot
	REBID->Divide(mcEBID);
	REBID->SetMarkerStyle(21);
	REBID->GetXaxis()->SetTitle("the distribution of EB photon's IDMVA");
	REBID->GetYaxis()->SetTitle("Ratio");
	
	TH1F *REEID = (TH1F*)EEID->Clone("REEID");
	REEID->SetLineColor(kBlack);
	REEID->SetMinimum(0);  // Define Y ..
	REEID->SetMaximum(rMax); // .. range
	REEID->Sumw2();
	REEID->SetStats(0);      // No statistics on lower plot
	REEID->Divide(mcEEID);
	REEID->SetMarkerStyle(21);
	REEID->GetXaxis()->SetTitle("the distribution of EE photon's IDMVA");
	REEID->GetYaxis()->SetTitle("Ratio");
	
	TH1F *REBCutID = (TH1F*)EBCutID->Clone("REBCutID");
	REBCutID->SetLineColor(kBlack);
	REBCutID->SetMinimum(0);  // Define Y ..
	REBCutID->SetMaximum(rMax); // .. range
	REBCutID->Sumw2();
	REBCutID->SetStats(0);      // No statistics on lower plot
	REBCutID->Divide(mcEBCutID);
	REBCutID->SetMarkerStyle(21);
	REBCutID->GetXaxis()->SetTitle("the distribution of EB photon's IDMVA(cut)");
	REBCutID->GetYaxis()->SetTitle("Ratio");
	
	TH1F *REECutID = (TH1F*)EECutID->Clone("REECutID");
	REECutID->SetLineColor(kBlack);
	REECutID->SetMinimum(0);  // Define Y ..
	REECutID->SetMaximum(rMax); // .. range
	REECutID->Sumw2();
	REECutID->SetStats(0);      // No statistics on lower plot
	REECutID->Divide(mcEECutID);
	REECutID->SetMarkerStyle(21);
	REECutID->GetXaxis()->SetTitle("the distribution of EE photon's IDMVA(cut)");
	REECutID->GetYaxis()->SetTitle("Ratio");
	
	TH1F *RSig = (TH1F*)Sig->Clone("RSig");
	RSig->SetLineColor(kBlack);
	RSig->SetMinimum(0);  // Define Y ..
	RSig->SetMaximum(5); // .. range
	RSig->Sumw2();
	RSig->SetStats(0);      // No statistics on lower plot
	RSig->Divide(mcSig);
	RSig->SetMarkerStyle(21);
	RSig->GetXaxis()->SetTitle("the distribution of photon's phoSigmaIEtaIEtaFull5x5");
	RSig->GetYaxis()->SetTitle("Ratio");
	
	TH1F *REBSig = (TH1F*)EBSig->Clone("REBSig");
	REBSig->SetLineColor(kBlack);
	REBSig->SetMinimum(0);  // Define Y ..
	REBSig->SetMaximum(rMax); // .. range
	REBSig->Sumw2();
	REBSig->SetStats(0);      // No statistics on lower plot
	REBSig->Divide(mcEBSig);
	REBSig->SetMarkerStyle(21);
	REBSig->GetXaxis()->SetTitle("the distribution of EB photon's phoSigmaIEtaIEtaFull5x5");
	REBSig->GetYaxis()->SetTitle("Ratio");
	
	TH1F *REESig = (TH1F*)EESig->Clone("REESig");
	REESig->SetLineColor(kBlack);
	REESig->SetMinimum(0);  // Define Y ..
	REESig->SetMaximum(rMax); // .. range
	REESig->Sumw2();
	REESig->SetStats(0);      // No statistics on lower plot
	REESig->Divide(mcEESig);
	REESig->SetMarkerStyle(21);
	REESig->GetXaxis()->SetTitle("the distribution of EE photon's phoSigmaIEtaIEtaFull5x5");
	REESig->GetYaxis()->SetTitle("Ratio");
	
	TH1F *REESigRR = (TH1F*)EESigRR->Clone("REESigRR");
	REESigRR->SetLineColor(kBlack);
	REESigRR->SetMinimum(0);  // Define Y ..
	REESigRR->SetMaximum(2); // .. range
	REESigRR->Sumw2();
	REESigRR->SetStats(0);      // No statistics on lower plot
	REESigRR->Divide(mcEESigRR);
	REESigRR->SetMarkerStyle(21);
	REESigRR->GetXaxis()->SetTitle("the distribution of EE photon's Preshower Sigma RR");
	REESigRR->GetYaxis()->SetTitle("Ratio");
	
	TH1F *REBEnRes = (TH1F*)EBEnRes->Clone("REBEnRes");
	REBEnRes->SetLineColor(kBlack);
	REBEnRes->SetMinimum(0);  // Define Y ..
	REBEnRes->SetMaximum(2); // .. range
	REBEnRes->Sumw2();
	REBEnRes->SetStats(0);      // No statistics on lower plot
	REBEnRes->Divide(mcEBEnRes);
	REBEnRes->SetMarkerStyle(21);
	REBEnRes->GetXaxis()->SetTitle("EB's photon energy resolution");
	REBEnRes->GetYaxis()->SetTitle("Ratio");
	
	TH1F *REEEnRes = (TH1F*)EEEnRes->Clone("REEEnRes");
	REEEnRes->SetLineColor(kBlack);
	REEEnRes->SetMinimum(0);  // Define Y ..
	REEEnRes->SetMaximum(2); // .. range
	REEEnRes->Sumw2();
	REEEnRes->SetStats(0);      // No statistics on lower plot
	REEEnRes->Divide(mcEEEnRes);
	REEEnRes->SetMarkerStyle(21);
	REEEnRes->GetXaxis()->SetTitle("EE's photon energy resolution");
	REEEnRes->GetYaxis()->SetTitle("Ratio");
	/*
	TH1F * = (TH1F*)->Clone("");
	->SetLineColor(kBlack);
	->SetMinimum(0);  // Define Y ..
	->SetMaximum(2); // .. range
	->Sumw2();
	->SetStats(0);      // No statistics on lower plot
	->Divide(mcZmass);
	->SetMarkerStyle(21);
	->GetXaxis()->SetTitle("");
	->GetYaxis()->SetTitle("Ratio");
	*/
	
	TCanvas *a = new TCanvas("a","a", 1800, 1200);
	a->Divide(3,2);
	TCanvas *a1 = new TCanvas("a1","a1", 1800, 1200);
	a1->Divide(3,2);
	TCanvas *b = new TCanvas("b","b", 1800, 1200);
	b->Divide(2,2);
	TCanvas *b1 = new TCanvas("b1","b1", 1800, 1200);
	b1->Divide(2,2);
	TCanvas *c = new TCanvas("c","c", 2400, 1200);
	c->Divide(3,1);
	TCanvas *c1 = new TCanvas("c1","c1", 2400, 1200);
	c1->Divide(3,1);
	TCanvas *RR = new TCanvas("RR","RR", 2000, 2000);
	RR->Divide(2,2);
	TCanvas *Rs = new TCanvas("Resolution","Resolution", 4000, 2000);
	Rs->Divide(4,2);

	
	TCanvas *d = new TCanvas("d","d", 1800, 1200);
	d->Divide(3,2);
	TCanvas *e = new TCanvas("e","e", 1800, 1200);
	e->Divide(2,2);
	TCanvas *f = new TCanvas("f","f", 2400, 1200);
	f->Divide(3,1);

	a->cd(1);
	Zmass->Sumw2(kTRUE);
	mcZmass->Sumw2(kFALSE);
	Zmass->Draw();
	mcZmass->Draw("SAME");
	a->cd(2);
	TagPt->Sumw2(kTRUE);
	mcTagPt->Sumw2(kFALSE);
	TagPt->Draw();
	mcTagPt->Draw("SAME");
	a->cd(3);
	ProPt->Sumw2(kTRUE);
	mcProPt->Sumw2(kFALSE);
	ProPt->Draw();
	mcProPt->Draw("SAME");
	a->cd(4);
	RZmass->Draw("ep"); // Draw the ratio plot
	a->cd(5);
	RTagPt->Draw("ep"); // Draw the ratio plot
	a->cd(6);
	RProPt->Draw("ep"); // Draw the ratio plot
	
	a1->cd(1);
	SCEta->Sumw2(kTRUE);
	mcSCEta->Sumw2(kFALSE);
	SCEta->Draw();
	mcSCEta->Draw("SAME");
	a1->cd(2);
	EBR9->Sumw2(kTRUE);
	mcEBR9->Sumw2(kFALSE);
	EBR9->Draw();
	mcEBR9->Draw("SAME");
	a1->cd(3);
	EER9->Sumw2(kTRUE);
	mcEER9->Sumw2(kFALSE);
	EER9->Draw();
	mcEER9->Draw("SAME");
	a1->cd(4);
	RSCEta->Draw("ep"); // Draw the ratio plot
	a1->cd(5);
	REBR9->Draw("ep"); // Draw the ratio plot
	a1->cd(6);
	REER9->Draw("ep"); // Draw the ratio plot

	b->cd(1);
	EBID->Sumw2(kTRUE);
	EBID->Draw();
	mcEBID->Sumw2(kFALSE);
	mcEBID->Draw("SAME");
	b->cd(2);
	EEID->Sumw2(kTRUE);
	EEID->Draw();  
	mcEEID->Sumw2(kFALSE);
	mcEEID->Draw("SAME");
	b->cd(3);
	REBID->Draw("ep"); // Draw the ratio plot
	b->cd(4);
	REEID->Draw("ep"); // Draw the ratio plot
	
	b1->cd(1);
	EBCutID->Sumw2(kTRUE);
	EBCutID->Draw();
	mcEBCutID->Sumw2(kFALSE);
	mcEBCutID->Draw("SAME");
	b1->cd(2);
	EECutID->Sumw2(kTRUE);
	EECutID->Draw();
	mcEECutID->Sumw2(kFALSE);
	mcEECutID->Draw("SAME");
	b1->cd(3);
	REBCutID->Draw("ep"); // Draw the ratio plot
	b1->cd(4);
	REECutID->Draw("ep"); // Draw the ratio plot
	
	c->cd(1);
	Sig->Sumw2(kTRUE);
	mcSig->Sumw2(kFALSE);
	Sig->Draw();
	mcSig->Draw("SAME");
	c->cd(2);
	EBSig->Sumw2(kTRUE);
	mcEBSig->Sumw2(kFALSE);
	EBSig->Draw();
	mcEBSig->Draw("SAME");
	c->cd(3);
	EESig->Sumw2(kTRUE);
	mcEESig->Sumw2(kFALSE);
	EESig->Draw();
	mcEESig->Draw("SAME");
	
	c1->cd(1);
	RSig->Draw("ep");
	c1->cd(2);
	REBSig->Draw("ep");
	c1->cd(3);
	REESig->Draw("ep");
	
	RR->cd(1);
	EESigRR->Sumw2(kTRUE);
	mcEESigRR->Sumw2(kFALSE);
	EESigRR->Draw();
	mcEESigRR->Draw("SAME");
	RR->cd(2);
	EESigRR->Sumw2(kTRUE);
	mcEESigRR->Sumw2(kFALSE);
	mcEESigRR->Draw();
	EESigRR->Draw("SAME");
	RR->cd(3);
	REESigRR->Draw("ep");
	
	EBEnRes->Sumw2(kTRUE);
	mcEBEnRes->Sumw2(kFALSE);
	EEEnRes->Sumw2(kTRUE);
	mcEEEnRes->Sumw2(kFALSE);
	Rs->cd(1);
	EBEnRes->Draw();
	mcEBEnRes->Draw("SAME");
	Rs->cd(2);
	EEEnRes->Draw();
	mcEEEnRes->Draw("SAME");
	Rs->cd(3);
	mcEBEnRes->Draw();
	EBEnRes->Draw("SAME");
	Rs->cd(4);
	mcEEEnRes->Draw();
	EEEnRes->Draw("SAME");
	Rs->cd(5);
	REEEnRes->Draw("ep");
	Rs->cd(6);
	REEEnRes->Draw("ep");

	d->cd(1);
	Zmass->Sumw2(kTRUE);
	mcZmass->Sumw2(kFALSE);
	mcZmass->Draw();
	Zmass->Draw("SAME");
	d->cd(2);
	TagPt->Sumw2(kTRUE);
	mcTagPt->Sumw2(kFALSE);
	mcTagPt->Draw();
	TagPt->Draw("SAME");
	d->cd(3);
	ProPt->Sumw2(kTRUE);
	mcProPt->Sumw2(kFALSE);
	mcProPt->Draw();
	ProPt->Draw("SAME");
	d->cd(4);
	SCEta->Sumw2(kTRUE);
	mcSCEta->Sumw2(kFALSE);
	mcSCEta->Draw();
	SCEta->Draw("SAME");
	d->cd(5);
	EBR9->Sumw2(kTRUE);
	mcEBR9->Sumw2(kFALSE);
	mcEBR9->Draw();
	EBR9->Draw("SAME");
	d->cd(6);
	EER9->Sumw2(kTRUE);
	mcEER9->Sumw2(kFALSE);
	mcEER9->Draw();
	EER9->Draw("SAME");

	e->cd(1);
	EBID->Sumw2(kTRUE);
	mcEBID->Sumw2(kFALSE);
	mcEBID->Draw();
	EBID->Draw("SAME");
	e->cd(2);
	EEID->Sumw2(kTRUE);
	mcEEID->Sumw2(kFALSE);
	mcEEID->Draw();
	EEID->Draw("SAME");
	e->cd(3);
	EBCutID->Sumw2(kTRUE);
	mcEBCutID->Sumw2(kFALSE);
	mcEBCutID->Draw();
	EBCutID->Draw("SAME");
	e->cd(4);
	EECutID->Sumw2(kTRUE);
	mcEECutID->Sumw2(kFALSE);
	mcEECutID->Draw();
	EECutID->Draw("SAME");
	
	f->cd(1);
	Sig->Sumw2(kTRUE);
	mcSig->Sumw2(kFALSE);
	mcSig->Draw();
	Sig->Draw("SAME");
	f->cd(2);
	EBSig->Sumw2(kTRUE);
	mcEBSig->Sumw2(kFALSE);
	mcEBSig->Draw();
	EBSig->Draw("SAME");
	f->cd(3);
	EESig->Sumw2(kTRUE);
	mcEESig->Sumw2(kFALSE);
	mcEESig->Draw();
	EESig->Draw("SAME");

	

	a->Print("GjA.png");
	a1->Print("GjA1.png");
	b->Print("GjB.png");
	b1->Print("GjB1.png");
	c->Print("GjC.png");
	c1->Print("GjC1.png");
	RR->Print("GjRR.png");
	Rs->Print("GjRs.png");
	d->Print("GjD.png");
	e->Print("GjE.png");
	f->Print("GjF.png");

	a->Print("GpA.pdf");
	a1->Print("GpA1.pdf");
	b->Print("GpB.pdf");
	b1->Print("GpB1.pdf");
	c->Print("GpC.pdf");
	c1->Print("GpC1.pdf");
	RR->Print("GpRR.pdf");
	Rs->Print("GpRs.pdf");
	d->Print("GpD.pdf");
	e->Print("GpE.pdf");
	f->Print("GpF.pdf");
}
