#include <fstream>
#include <iostream>
#include <vector>
#include <TLorentzVector.h>
#include <TH1D.h>
#include <TRandom.h>
#include <TLegend.h>
#include <TCanvas.h>
#include </afs/cern.ch/work/j/jhihjia/private/untuplizer_07.h>
using namespace std;

void ProEle(TreeReader &data, vector<int> &TagNumber, vector<int> &ProNumber, vector<int> &ZNumber){

  Int_t nEle = data.GetInt("nEle");
  Float_t* elePt      = data.GetPtrFloat("elePt");
  Float_t* eleEta     = data.GetPtrFloat("eleEta");
  Float_t* elePhi     = data.GetPtrFloat("elePhi");

  TLorentzVector Z, eleTag, elePro;
  Float_t ZMass = 91.2, mini = 0;
  Float_t TemProNum = 0, TemZMass = 0;

  if(TagNumber.size() != 0){
    eleTag.SetPtEtaPhiM(elePt[TagNumber[0]], eleEta[TagNumber[0]], elePhi[TagNumber[0]], 0.000511);
    for(int i = 0; i < nEle; i++){
      if(elePt[i] > 15){
        elePro.SetPtEtaPhiM(elePt[i], eleEta[i], elePhi[i], 0.000511);
        Z = eleTag + elePro;
        if(Z.M() > 80 && Z.M() < 100){
          ProNumber.push_back(i);
          ZNumber.push_back(Z.M());
        }
      }
    } // find pro ele
    if(ProNumber.size() > 1){
      TemProNum = ProNumber[0];
      TemZMass = ZNumber[0];
      mini = fabs(TemZMass-ZMass);
      for(int i = 1; i < ProNumber.size(); i++){
        if(mini > fabs(ZNumber[i]-ZMass)){
  	  TemProNum = ProNumber[i];
          TemZMass = ZNumber[i];
          mini = fabs(ZNumber[i]-ZMass);
        }
      }
      ProNumber.clear(), ZNumber.clear();
      ProNumber.push_back(TemProNum);
      ZNumber.push_back(TemZMass);
    } // find the one probe
  }
}
